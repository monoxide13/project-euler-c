/*

The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:
13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.

Ans: 837799

*/

// g++ -std=c++11 prog.cpp && ./a.out

#include "../headers.h"

using namespace std;

int main(){
	unsigned long int longestInt=0, longestChain=0, currentInt, currentChain;
	for(int x=2; x<1000000; ++x){
		currentInt=x;
		currentChain=1;
		while(currentInt>1){
			if(currentInt%2==0)
				currentInt=currentInt/2;
			else
				currentInt=currentInt*3+1;
			++currentChain;
		}
		if(currentChain>longestChain){
			longestChain=currentChain;
			longestInt=x;
		}
		//cout << "Int:" << x << "  Chain:" << currentChain << endl;
	}
	cout << "LongestInt: " << longestInt << "   LongestChain: " << longestChain << endl;
}