/*


By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

3
7 4
2 4 6
8 5 9 3

That is, 3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom in triangle.txt (right click and 'Save Link/Target As...'), a 15K text file containing a triangle with one-hundred rows.

NOTE: This is a much more difficult version of Problem 18. It is not possible to try every route to solve this problem, as there are 299 altogether! If you could check one trillion (1012) routes every second it would take over twenty billion years to check them all. There is an efficient algorithm to solve it. ;o)

Ans: 7273

*/

// g++ -std=c++11 prog.cpp && ./a.out

#include "../headers.h"

using namespace std;

struct astarStruct{
	unsigned long value;
	unsigned long sum;
};

struct astarStruct * loadItems(unsigned int);
void processItems(unsigned int, struct astarStruct *, struct astarStruct *);
char fileName[] = "triangle.txt";

ifstream ifs;

int main(){
	struct astarStruct * previous = NULL;
	struct astarStruct * current = NULL;
	unsigned int rowCount=1;

	ifs.open("triangle.txt", ifstream::in);
	if(!ifs.is_open())
		exit(1);

/*
 * First: load the items into an array of astarStructs.
 * Second: process the array, and compare it with the previous values.
 * 	Add which previous parent was larger.
 * Third: move pointers to prepare for next row.
*/
	while(1){ // Outer loop that increments through the rows.
		current = loadItems(rowCount); // Load the files into an array
		if(current==NULL)
			break;
		if(rowCount==1){
			current[0].sum=current[0].value;
		}else{
			processItems(rowCount, previous, current);
		}
		int i;
		for(i=0; i<rowCount; ++i){
			cout << current[i].sum << " ";
		}
		delete[] previous;
		previous=current;
		cout << endl;
		++rowCount;
	}
	unsigned long max=0, i;
	for(i=0; i<rowCount-1; ++i){
		if(previous[i].sum > max)
			max = previous[i].sum;
	}
	cout << endl << "Answer: " << max << endl;
	ifs.close();
}

struct astarStruct * loadItems(unsigned int row){
	struct astarStruct * array = new struct astarStruct[row];
	// Load row into a temp string
	char s[row*3];
	ifs.getline(s, row*3);
	if(ifs.gcount()==0)
		return NULL;
	int i;
	char * pEnd = s;
	for(i=0; i<row; ++i){
		array[i].value = strtol(pEnd, &pEnd, 10);
	}
	return array;
}

void processItems(unsigned int row, struct astarStruct * previous, struct astarStruct * current){
	int i;
	current[0].sum = current[0].value+previous[0].sum; // Process the first object
	current[row-1].sum = current[row-1].value+previous[row-2].sum; // Process the last object
	for(i=1; i<(row-1); ++i){ // For all the objects in the middle
		if(previous[i-1].sum > previous[i].sum)
			current[i].sum = current[i].value+previous[i-1].sum;
		else
			current[i].sum = current[i].value+previous[i].sum;
	}
}


