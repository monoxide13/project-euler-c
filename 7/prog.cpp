/*
 
The sum of the squares of the first ten natural numbers is,
12 + 22 + ... + 102 = 385

The square of the sum of the first ten natural numbers is,
(1 + 2 + ... + 10)2 = 552 = 3025

Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

Ans: 104743

*/

// g++ prog.cpp ../libs/factorial.cpp && ./a.out

#include "../headers.h"
#include "../libs/factorial.h"

using namespace std;

int main(){
	int primeCount=2, primeNumber=3;
	while(primeCount<10001){
		double ** ptr = new double *;
		int * size = new int;
		int prime = factorizer(++++primeNumber, ptr, size);
		if(prime==0){
			++primeCount;
			//cout << primeCount << "-" << primeNumber << endl;
		}
		delete(ptr);
		delete(size);
	}
	cout << "Prime: " << primeNumber << endl;
}