/*

215 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

What is the sum of the digits of the number 21000?

Ans: 1366

*/

// g++ -std=c++11 prog.cpp && ./a.out

#include "../headers.h"

using namespace std;

const int arraySize=320;
const int power=1000;

int main(){
	unsigned char array[arraySize];
	memset(array, 0, sizeof(char)*arraySize);
	array[0]=2;
	for(int i=1; i<power; ++i){
		int carry=0;
		int temp=0;
		for(int j=0; j<arraySize; ++j){
			temp=array[j]*2+carry;
			if(temp>=10){
				carry=1;
				temp-=10;
				if(j==arraySize-1)
					cout << endl << "OVERFLOW" << endl;
			}else
				carry=0;
			array[j]=temp;
			//cout << temp;
		}
		
		//cout << endl;
	}
	unsigned int sum=0;
	for(int i=arraySize-1; i>=0; --i){
		cout << (unsigned int)array[i];
		sum+=(unsigned int)array[i];
	}
	cout << endl << "Answer: " << sum << endl;
}