/*

If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?

NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.

Ans: 1366

*/

// g++ -std=c++11 prog.cpp && ./a.out

#include "../headers.h"

using namespace std;


unsigned int getCharLength(unsigned int);


int main(){
	unsigned long sum=0, temp;
	for(unsigned int x=1; x<=1000; ++x){
		temp=0;
		if(x==1000){
			sum+=11; // one thousand
			cout << x << "=" << 11 << endl;
			break;
		}
		if(x>=100){
			temp+=getCharLength((unsigned int)floor(x/100))+7; // hundred
				if(x%100!=0)
					temp+=3; // and
			}
		switch(x%100-x%10){
			case(0):
				break;
			case(10): // ten handled elsewhere
				break;
			case(40): // forty
				temp+=5;
				break;
			case(50): // fifty
				temp+=5;
				break;
			case(60): // sixty
				temp+=5;
				break;
			case(70): // seventy
				temp+=7;
				break;
			default: // others have 6 leters
				temp+=6;
				break;
		}
		switch(x%100-x%10){
			case(10):
				switch(x%10){
					case(0): // ten
						temp+=3;
						break;
					case(1): // eleven
						temp+=6;
						break;
					case(2): // twelve
						temp+=6;
						break;
					case(3): // thirteen
						temp+=8;
						break;
					case(4): // thirteen
						temp+=8;
						break;
					case(5): // fifteen
						temp+=7;
						break;
					case(6): // sixteen
						temp+=7;
						break;
					case(7): // seventeen
						temp+=9;
						break;
					case(8): // eighteen
						temp+=8;
						break;
					case(9): // nineteen
						temp+=8;
						break;
				}
				break;
			default:
				temp+=getCharLength((unsigned int)x%10);
				break;
		}
		cout << x << "=" << temp << endl;
		sum+=temp;
	}
	cout << endl << "Answer: " << sum << endl;
}

unsigned int getCharLength(unsigned int input){
	unsigned int temp=0;
	switch(input){
		case(1):
			temp=3;
			break;
		case(2):
			temp=3;
			break;
		case(3):
			temp=5;
			break;
		case(4):
			temp=4;
			break;
		case(5):
			temp=4;
			break;
		case(6):
			temp=3;
			break;
		case(7):
			temp=5;
			break;
		case(8):
			temp=5;
			break;
		case(9):
			temp=4;
			break;
	}
	return temp;
}