// This function calculates factorials and returns an array.
// Inputs: [Number] to be looked at. [double **] that will point to the array location. [Array Size] that is the size of array allocated.
// Returns: [Factorials calcuated]. 0 indicates prime.
// Note that the array size is the actual size of the allocated array. The return value is the number of factorials. Array size will always be equal or larger to factorials.

#include "factorial.h"


const int initialSize=10;
const int factorialArrayInc = 10;

double * factorizerRealloc(double *, int *);

/*
using namespace std;

int main(){
	double ** ptr = new double *;
	int * size = new int(initialSize);
	int retval = factorial(40,ptr,size);
	cout << "Size=" << *size << endl << "Values:" << endl;
//	for(int x=0; x<*size; x++){
//		cout << (*ptr)[x] << endl;
//	}
	for(int x=1; x<=retval; x++){
		cout << (*ptr)[x-1] << endl;
	}
}
*/


int factorizer(double number, double ** ptr, int * size){
	// Initially allocate an array of doubles.
	double * newPtr = new (std::nothrow) double[initialSize];
	*size=initialSize;
	if (newPtr==NULL){
		std::cout << "Error allocating memory" << std::endl;
		return -1;
	}
	int factorials=0;
	double midpoint=floor(sqrt(number));
	int quotient=0;
	for(double a=2; a<=midpoint; ++a){
		if (0!=fmod(number, a)) continue;
		if(factorials+2>*size){
			newPtr = factorizerRealloc(newPtr, size);
			if(newPtr==0) return -1;
		}
		newPtr[factorials++]=a;
		if(a!=number/a) newPtr[factorials++]=number/a;	
	}
	*ptr=newPtr;
	return factorials;
}

double * factorizerRealloc(double * ptr, int * size){
	double * newPtr = new (std::nothrow) double[*size + factorialArrayInc];
	if (newPtr==NULL){
		std::cout << "Error allocating memory for array increase" << std::endl;
		return 0;
	}
	std::copy (ptr, ptr+*size, newPtr);
	delete (ptr);
	*size = *size + factorialArrayInc;
	return newPtr;
}

unsigned long long int factorial(unsigned int factor){
	unsigned long long int result=1;
	for(int x=factor; x>1; --x){
		result*=x;
	}
	return result;
}