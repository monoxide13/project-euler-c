
#include "palindrome.h"

bool isPalindrome(std::string input){
	for(int x=0; x<input.length()/2; ++x){
		if(input[x] != input[input.length()-x-1])
			return false;
	}
	return true;
}