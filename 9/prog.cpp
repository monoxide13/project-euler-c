/*

A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
a2 + b2 = c2

For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.

Ans: 31875000

*/

// g++ prog.cpp -std=c++11 && ./a.out

#include "../headers.h"


using namespace std;

int main(){
	for(int a=1; a<1000; ++a){
		for(int b=1000-a; b>0; --b){
			if(a+b>=1000) continue;
			int c=1000-a-b;
			if(a*a + b*b == c*c){
				cout << "Solution: A=" << a << " B=" << b << " C=" << c << endl << "Product: " << a*b*c << endl;
			}
		}
	}
	return 0;
}