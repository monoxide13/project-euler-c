/*

A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.

Ans: 906609

*/

// g++ prog.cpp ../libs/palindrome.cpp -std=c++11 && ./a.out

#include "../headers.h"
#include "../libs/palindrome.h"

using namespace std;

int main(){
	int largestVal;
	for(int a=999; a>=900; --a){
		for(int b=999; b>=a; --b){
			if(isPalindrome(to_string(a*b)) && a*b>largestVal){
				largestVal=a*b;
			}
		}
	}
	cout << "Largest Value: " << largestVal << endl;
}