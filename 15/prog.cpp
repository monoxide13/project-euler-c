/*

Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.

How many such routes are there through a 20×20 grid?

//  Using math instead of calculating the routes.

nCr=nPr/r!
nPr=n!/(n-r)!

40!/(40-20)!/20!
40!/(20!*20!)
(40*39..22*21)/(20*19..2*1)
260*37*11*31*29*3*23*21=...

Ans: 137846528820

*/

// g++ -std=c++11 prog.cpp && ./a.out

#include "../headers.h"

using namespace std;

const int n=40;
const int r=20;

int main(){
	unsigned long long int result=0;
	result=260*37*11*31; // Overflow if all multiplication is done in one line.
	result*=29*3*23*21;
	cout << "Answer: " << result << endl;
}