/*

2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

2*3=6. So all answers must be divisible by 6. Add 6 to itself until I get a number divisible by 4. 24.
Continue this until 20...

Ans: 232792560

*/

#include "../headers.h"


using namespace std;

int main(){
	int value=1, lastvalue=1;
	for(int a=1; a<=20; ++a){
		while(value%a != 0){
			value+=lastvalue;
		}
		lastvalue=value;
	}
	cout << "Largest Value: " << lastvalue << endl;
}