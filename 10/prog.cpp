/*
 
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.

Ans: 142913828922

*/

// g++ -std=c++11 prog.cpp ../libs/factorial.cpp && ./a.out

#include "../headers.h"
#include "../libs/factorial.h"

using namespace std;

int main(){
	unsigned long long int sum=5;
	int primeCount=2, primeNumber=3;
	while(primeNumber<2000000){
		double ** ptr = new double *;
		int * size = new int;
		int prime = factorial(++++primeNumber, ptr, size);
		if(prime==0){
			++primeCount;
			sum+=primeNumber;
			//cout << primeCount << "-" << primeNumber << endl;
		}
		delete(ptr);
		delete(size);
	}
	cout <<fixed<< "Sum: " << sum << endl;
}