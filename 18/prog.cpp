/*



By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.

3
7 4
2 4 6
8 5 9 3

That is, 3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom of the triangle below:

75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23

NOTE: As there are only 16384 routes, it is possible to solve this problem by trying every route. However, Problem 67, is the same challenge with a triangle containing one-hundred rows; it cannot be solved by brute force, and requires a clever method! ;o)


Ans: 1074

*/

// g++ -std=c++11 prog.cpp && ./a.out

#include "../headers.h"

using namespace std;

struct astarStruct{
	unsigned long value;
	unsigned long sum;
};

struct astarStruct * loadItems(unsigned int);
void processItems(unsigned int, struct astarStruct *, struct astarStruct *);
char fileName[] = "triangle.txt";

ifstream ifs;

int main(){
	struct astarStruct * previous = NULL;
	struct astarStruct * current = NULL;
	unsigned int rowCount=1;

	ifs.open("triangle.txt", ifstream::in);
	if(!ifs.is_open())
		exit(1);

/*
 * First: load the items into an array of astarStructs.
 * Second: process the array, and compare it with the previous values.
 * 	Add which previous parent was larger.
 * Third: move pointers to prepare for next row.
*/
	while(1){ // Outer loop that increments through the rows.
		current = loadItems(rowCount); // Load the files into an array
		if(current==NULL)
			break;
		if(rowCount==1){
			current[0].sum=current[0].value;
		}else{
			processItems(rowCount, previous, current);
		}
		int i;
		for(i=0; i<rowCount; ++i){
			cout << current[i].sum << " ";
		}
		delete[] previous;
		previous=current;
		cout << endl;
		++rowCount;
	}
	unsigned long max=0, i;
	for(i=0; i<rowCount-1; ++i){
		if(previous[i].sum > max)
			max = previous[i].sum;
	}
	cout << endl << "Answer: " << max << endl;
	ifs.close();
}

struct astarStruct * loadItems(unsigned int row){
	struct astarStruct * array = new struct astarStruct[row];
	// Load row into a temp string
	char s[row*3];
	ifs.getline(s, row*3);
	if(ifs.gcount()==0)
		return NULL;
	int i;
	char * pEnd = s;
	for(i=0; i<row; ++i){
		array[i].value = strtol(pEnd, &pEnd, 10);
	}
	return array;
}

void processItems(unsigned int row, struct astarStruct * previous, struct astarStruct * current){
	int i;
	current[0].sum = current[0].value+previous[0].sum; // Process the first object
	current[row-1].sum = current[row-1].value+previous[row-2].sum; // Process the last object
	for(i=1; i<(row-1); ++i){ // For all the objects in the middle
		if(previous[i-1].sum > previous[i].sum)
			current[i].sum = current[i].value+previous[i-1].sum;
		else
			current[i].sum = current[i].value+previous[i].sum;
	}
}


