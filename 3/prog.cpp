/*

The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?

Ans: 6857
*/

// g++ prog.cpp ../libs/factorial.cpp && ./a.out

#include "../headers.h"
#include "../libs/factorial.h"

using namespace std;


int main(){
	// Calculate all the factorials of 600851475143
	double ** ptr = new double *;
	int * size = new int;
	int retval = factorizer(600851475143,ptr,size);
	cout << "Factorials=" << retval << endl;
	for(int x=1; x<=retval; x++){
		//cout << (*ptr)[x-1] << endl;
	}
	// Look at the individual factorials, and calculate if they are prime.
	for(int x=1; x<=retval; x++){
		double ** sptr = new double *;
		int * ssize = new int;
		int prime = factorial(((*ptr)[x-1]),sptr,ssize);
		//cout << (*ptr)[x-1] << "->" << prime << endl;
		if(prime==0){
			cout << "Prime:" << (*ptr)[x-1] << endl;
		}
		delete(sptr);
		delete(ssize);
	}
	delete(ptr);
	delete(size);
	return 0;
}